# Indecision App
A simple app made with React - this app will make your decision from your choice. (_It is my First React App_)

## Demo
* link : https://ritwickdey.github.io/indecision-app/


> This repo is part of my [React Course](https://www.udemy.com/react-2nd-edition/) (by Andrew Mead)